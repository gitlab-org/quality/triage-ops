# frozen_string_literal: true

module CsmermIssueTriageHelper
  class << self
    REQUIRED_LABELS = [
      {
        field: 'Priority label',
        example: '~"Priority::Top-5"',
        check: ->(labels) { labels.any? { |l| l.start_with?("Priority::") } }
      },
      {
        field: 'Team label',
        example: '~"Team::CSM"',
        check: ->(labels) { labels.any? { |l| l.start_with?("Team::") } }
      },
      {
        field: 'Status/At Risk label',
        example: '~"SP Objective::Status::On Track"',
        check: ->(labels) { labels.any? { |l| l.start_with?("SP Objective::Status::") } }
      },
      {
        field: 'Region label',
        example: '~"CS Region::AMER"',
        check: ->(labels) { labels.any? { |l| l.start_with?("CS Region::") } }
      },
      {
        field: 'OKR label',
        example: '~"OKR::Yes"',
        check: ->(labels) { labels.any? { |l| l.start_with?("OKR::") } }
      },
      {
        field: 'LT Sponsorship label',
        example: '~"CSLT::Kent"',
        check: ->(labels) { labels.any? { |l| l.start_with?("CSLT::") } }
      },
      {
        field: 'Fiscal quarter label',
        example: '~"FY24-Q1"',
        check: ->(labels) { labels.any? { |l| l.match?(/^FY\d{2}-Q[1-4]$/) } }
      },
      {
        field: 'XLT DRI label',
        example: '~"CSXLT::Oliver"',
        check: ->(labels) { labels.any? { |l| l.start_with?("CSXLT::") } }
      }
    ].freeze

    def csmerm_issue_triage_automated_comment_table(resource)
      <<~MARKDOWN.chomp
        | Missing Field | Example |
        |---------------| ------- |
        #{missing_csmerm_issue_attributes(resource).join("\n")}
      MARKDOWN
    end

    def missing_csmerm_issue_attributes(resource)
      missing_attributes = []

      # Check assignee
      missing_attributes << format_requirement('Issue assignee', '`@gavinpeltz`') if resource[:assignees].empty?

      # Check labels
      labels = resource["labels"]
      REQUIRED_LABELS.each do |req|
        missing_attributes << format_requirement(req[:field], req[:example]) unless req[:check].call(labels)
      end

      missing_attributes
    end

    private

    def format_requirement(field, example)
      "| #{field} | #{example} |"
    end
  end
end
