# frozen_string_literal: true

require 'gitlab'
require 'net/http'

class StaleResources
  COM_GITLAB_API = 'https://gitlab.com/api/v4'
  MAX_WAIT_TIME = 3600
  GITLAB_BOT_USERNAME = 'gitlab-bot'

  Gitlab.configure do |config|
    config.endpoint = COM_GITLAB_API
  end

  def self.stale?(project_path:, token:, project_id:, resource_iid:, days:, resource_type: :issue)
    new(
      project_path: project_path, token: token, project_id: project_id,
      resource_iid: resource_iid, resource_type: resource_type
    ).days_since_last_human_update > days
  end

  def self.active?(project_path:, token:, project_id:, resource_iid:, days:, resource_type: :issue)
    new(
      project_path: project_path, token: token, project_id: project_id,
      resource_iid: resource_iid, resource_type: resource_type
    ).days_since_last_human_update <= days
  end

  def initialize(project_path:, token:, project_id:, resource_iid:, resource_type: :issue)
    raise ArgumentError, 'An API token is needed!' if token.nil?

    @project_path = project_path
    @project_id = project_id
    @resource_iid = resource_iid
    @resource_type = resource_type
    @client = Gitlab.client(private_token: token)
  end

  def days_since_last_human_update
    age = -1
    notes = client.send("#{resource_type}_notes".to_sym, project_id, resource_iid, { order_by: "updated_at", sort: "desc" })
    notes.each do |note|
      next if note['author']['username'] == GITLAB_BOT_USERNAME

      age = age_of_timestamp(note['updated_at'])
      break
    end

    if age == -1 # If there are no comments at all, take the issue_created_date
      resource = client.send(resource_type, project_id, resource_iid)
      age = age_of_timestamp(resource['created_at'])
    end

    age
  end

  private

  def age_of_timestamp(timestamp, from: Date.today)
    (from - Date.parse(timestamp)).to_i
  end

  attr_reader :project_path, :client, :project_id, :resource_iid, :resource_type
end
