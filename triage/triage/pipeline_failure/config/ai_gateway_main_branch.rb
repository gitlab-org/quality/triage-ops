# frozen_string_literal: true

require_relative 'base'

module Triage
  module PipelineFailure
    module Config
      class AiGatewayMainBranch < Base
        def self.match?(event)
          event.project_path_with_namespace == 'gitlab-org/modelops/applied-ml/code-suggestions/ai-assist' &&
            event.ref == 'main'
        end

        def default_slack_channels
          %w[g_ai_framework g_mlops-alerts ai-infrastructure]
        end

        def auto_triage?
          true
        end
      end
    end
  end
end
