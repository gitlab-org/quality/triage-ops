# frozen_string_literal: true
require 'gitlab/objectified_hash'

module Triage
  class User < Gitlab::ObjectifiedHash
  end
end
