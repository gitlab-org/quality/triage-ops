# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/devops_labels_validator'
require_relative '../../job/devops_labels_nudger_job'

module Triage
  module Workflow
    class DevopsLabelsNudger < Processor
      react_to 'issue.open', 'issue.update'

      def applicable?
        # we only consider `issue.open` events applicable for `process`
        # `issue.update` was only added to run `processor_comment_cleanup` when label gets updated
        event.new_entity? &&
          event.from_gitlab_org_gitlab? &&
          event.by_team_member? &&
          unique_comment.no_previous_comment? &&
          !validator.labels_set?
      end

      def comment_cleanup_applicable?
        unique_comment.previous_comment? && validator.labels_set?
      end

      def process
        DevopsLabelsNudgerJob.perform_in(Triage::DEFAULT_ASYNC_DELAY_MINUTES, event)
      end

      def processor_comment_cleanup
        unique_comment.delete_previous_comment
      end

      def documentation
        <<~TEXT
          This processor posts a ownership label reminder to issues which are in progress or scheduled to start in 90 days.
        TEXT
      end

      private

      def validator
        @validator ||= DevopsLabelsValidator.new(event)
      end

      def unique_comment
        @unique_comment ||= validator.unique_comment
      end
    end
  end
end
