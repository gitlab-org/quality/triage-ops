# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  class FailedPipelineHelp < CommunityProcessor
    react_to 'pipeline.failed'

    def applicable?
      wider_community_contribution? &&
        event.latest_pipeline_in_merge_request? &&
        triggered_by_an_assignee? &&
        unique_comment.no_previous_comment?
    end

    def process
      comment = <<~MARKDOWN.concat("\n")
        Hey @#{event.event_actor_username}! Looks like your pipeline failed :confused:.
        If you need help fixing it, you can reply with: `@gitlab-bot help` to tag a merge
        request coach or you can ask for help in the #contribute channel on our
        [Discord community server](https://discord.gg/gitlab).
      MARKDOWN
      merge_request_path = "/projects/#{event.project_id}/merge_requests/#{event.merge_request_iid}"
      add_comment(unique_comment.wrap(comment), merge_request_path, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor automatically adds a comment to a community contribution when a pipeline fails.
        The comment instructs contributors to use GitLab-bot to tag a MR coach.
      TEXT
    end

    private

    def label_names
      @label_names ||= event.merge_request.labels
    end

    def triggered_by_an_assignee?
      event.merge_request.assignees.any? { |assignee| assignee['username'] == event.event_actor_username }
    end

    def unique_comment
      UniqueComment.new(
        self.class.name,
        event,
        unique_comment_identifier,
        noteable_object_kind: 'merge_request',
        noteable_resource_iid: event.merge_request_iid
      )
    end

    def unique_comment_identifier
      "#{self.class.name.demodulize} - Project ID #{event.project_id} - MR ID #{event.merge_request_iid}"
    end
  end
end
