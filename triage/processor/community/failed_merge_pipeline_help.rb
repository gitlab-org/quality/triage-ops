# frozen_string_literal: true

require_relative 'community_processor'
require_relative '../../../lib/merge_request_coach_helper'

module Triage
  class FailedMergePipelineHelp < CommunityProcessor
    include MergeRequestCoachHelper

    react_to 'pipeline.failed'

    def applicable?
      wider_community_contribution? &&
        event.latest_pipeline_in_merge_request? &&
        event.merge_train_pipeline?
    end

    def process
      comment = <<~MARKDOWN
        Hey #{coach}! Merge train pipeline failed for this MR.
        Please help to resolve the failure and deliver this contribution, thanks!
      MARKDOWN
      merge_request_path = "/projects/#{event.project_id}/merge_requests/#{event.merge_request_iid}"
      add_comment(comment, merge_request_path, append_source_link: true)
    end

    def documentation
      <<~TEXT
        This processor automatically assigns MR coach when a merge train pipeline fails.
        The comment asks coach to assist in resolving failure as contributor doesn't have rights to trigger merge train pipelines.
      TEXT
    end

    private

    def coach
      mr_reviewer || super
    end

    def mr_reviewer
      reviewers = event.merge_request.reviewers
      return nil if reviewers.blank?

      "@#{reviewers.first['username']}"
    end

    def group_label
      (Hierarchy::Group.all_labels & label_names).first
    end

    def label_names
      @label_names ||= event.merge_request.labels
    end
  end
end
