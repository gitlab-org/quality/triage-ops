# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class NudgeForMissingAutoMerge < Processor
    react_to 'pipeline.success'

    MR_DETAILED_STATUS_REQUIRING_MANUAL_ACTION = %w[
      conflict
      discussions_not_resolved
      merge_request_blocked
      need_rebase
    ].freeze

    def applicable?
      event.from_gitlab_org_gitlab? &&
        event.merged_results_pipeline? &&
        with_applicable_pipeline_name &&
        event.latest_pipeline_in_merge_request? &&
        unique_comment.no_previous_comment?
    end

    def process
      nudge_missing_auto_merge
    end

    def documentation
      <<~TEXT
        Nudges the latest reviewer to ask whether they forgot to set the MR to auto-merge.
      TEXT
    end

    private

    def with_applicable_pipeline_name
      event.name&.include?('tier:3')
    end

    def nudge_missing_auto_merge
      auto_merge_is_set     = associated_merge_request.merge_when_pipeline_succeeds
      detailed_merge_status = associated_merge_request.detailed_merge_status
      message_markdown      = nil

      if auto_merge_is_set && MR_DETAILED_STATUS_REQUIRING_MANUAL_ACTION.include?(detailed_merge_status)
        message_markdown = <<~MARKDOWN.chomp
          Hey there :wave:, could you please make sure this merge request gets merged?

          The merge request is set to auto-merge, but it is not currently mergeable (MR [`detailed_merge_status`](https://docs.gitlab.com/ee/api/merge_requests.html#merge-status) is **#{detailed_merge_status}**).
        MARKDOWN
      end

      return unless message_markdown

      comment = unique_comment.wrap(message_markdown).strip
      add_comment(comment, event.merge_request_path, append_source_link: true)
    end

    def unique_comment
      # Ensure we comment to the merge request associated to the pipeline from the event
      @unique_comment ||= UniqueComment.new(
        self.class.name,
        noteable_object_kind: 'merge_request',
        noteable_resource_iid: associated_merge_request.iid,
        noteable_project_id: associated_merge_request.target_project_id
      )
    end

    def associated_merge_request
      @associated_merge_request ||= event.merge_request
    end
  end
end
