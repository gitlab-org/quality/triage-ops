# frozen_string_literal: true

require_relative '../lib/growth_refine_automation_helper'

Gitlab::Triage::Resource::Context.include GrowthRefineAutomationHelper
