# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/triage/need_mr_approved_label'

RSpec.describe Triage::NeedMrApprovedLabel, :clean_cache do
  let(:instance) { described_class.new(event) }
  let(:labels_from_api)   { [] }
  let(:labels_from_event) { [] }
  let(:merge_request_from_api) do
    {
      'iid' => merge_request_iid,
      'labels' => labels_from_api
    }
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  include_context 'with event', Triage::MergeRequestEvent do
    let(:merge_request_iid) { 300 }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        maybe_automation_author?: false,
        jihu_contributor?: false,
        label_names: labels_from_event,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}",
      response_body: merge_request_from_api)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#need_mr_approved_label?' do
    subject { instance.need_mr_approved_label? }

    context 'when the `pipeline::expedited` label is only set on the merge request API' do
      let(:labels_from_api)   { ['pipeline::expedited'] }
      let(:labels_from_event) { [] }

      it { is_expected.to be_falsey }
    end

    context 'when the `pipeline::expedited` label is only set on the event' do
      let(:labels_from_api)   { [] }
      let(:labels_from_event) { ['pipeline::expedited'] }

      # We take the labels present on the MR, even if there are no labels on the MR
      it { is_expected.to be_truthy }
    end

    context 'when the `pipeline::expedited` label is set on both the MR api and the event' do
      let(:labels_from_api)   { ['pipeline::expedited'] }
      let(:labels_from_event) { ['pipeline::expedited'] }

      context 'when we cannot fetch the pipeline::expedited label from the API' do
        # rubocop:disable RSpec/VerifiedDoubles -- GitLab gem internal class
        let(:error_response_double) do
          double('Response',
            code: 500,
            parsed_response: '500 internal server error',
            request: double('Request Double', base_uri: '', path: 'projects/1/merge_requests/'))
        end
        # rubocop:enable RSpec/VerifiedDoubles

        before do
          allow(Triage.api_client).to receive(:merge_request).and_raise(
            Gitlab::Error::InternalServerError, error_response_double
          )
        end

        it 'falls back to the labels from the event' do
          expect(event).to receive(:label_names)

          expect(subject).to be_falsey
        end
      end
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      before do
        allow(event).to receive(:source_branch_is?).with(described_class::UPDATE_GITALY_BRANCH).and_return(true)
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only docs, and templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            },
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains a mixture of docs and non-docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      it { is_expected.to be_truthy }
    end
  end
end
