# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/anthropic_api'

RSpec.describe AnthropicApi do
  before do
    stub_env('ANTHROPIC_API_KEY' => 'anthropic-api-key')
    stub_request(:post, described_class::ANTHROPIC_API_URL)
      .with(
        body: read_fixture('anthropic_api_request.json'),
        headers: {
          'Content-Type' => 'application/json',
          'X-Api-Key' => 'anthropic-api-key'
        }
      ).to_return(
        status: 200,
        body: read_fixture('anthropic_api_response.json'),
        headers: { 'Content-Type' => 'application/json' }
      )
  end

  it 'makes the expected request and handles the response as expected' do
    expect(described_class.execute('hello', 'world')).to eq('Hello world!')
  end
end
