# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/constants/labels'
require_relative '../../lib/growth_time_in_dev_helper'

RSpec.describe GrowthTimeInDevHelper do
  let(:issue_resource) do
    Struct.new(:resource) do
      include GrowthTimeInDevHelper
    end
  end

  let(:project_id) { 1234 }
  let(:iid) { 5678 }
  let(:resource) { { project_id: project_id, iid: iid } }
  let(:in_dev_label) { Labels::WORKFLOW_IN_DEV_LABEL }
  let(:refinement_label) { Labels::WORKFLOW_REFINEMENT_LABEL }

  subject(:issue_resource_object) { issue_resource.new(resource) }

  before do
    allow(Triage.api_client)
      .to receive(:issue_label_events)
      .with(project_id, iid)
      .and_return(Gitlab::PaginatedResponse.new(label_events))
  end

  shared_examples 'time_in_dev_calculation' do
    let(:one_day) { 24 * 60 * 60 }
    let(:one_day_ago) { Time.now.utc - one_day }

    context 'when it is outside of the range' do
      context 'when one in dev label event' do
        let(:label_events) do
          [
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: outside_the_range.to_s,
              action: 'add'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: refinement_label },
              created_at: within_the_range.to_s,
              action: 'add'
            })
          ]
        end

        it 'returns false' do
          expect(issue_resource_object.public_send(method_name)).to be false
        end
      end

      context 'when two in dev label events' do
        let(:label_events) do
          [
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: within_the_range.to_s,
              action: 'add'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: outside_the_range.to_s,
              action: 'remove'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: refinement_label },
              created_at: within_the_range.to_s,
              action: 'add'
            })
          ]
        end

        it 'returns false' do
          expect(issue_resource_object.public_send(method_name)).to be false
        end
      end
    end

    context 'when it falls within the range' do
      context 'when one in dev label event' do
        let(:label_events) do
          [
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: within_the_range.to_s,
              action: 'add'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: refinement_label },
              created_at: outside_the_range.to_s,
              action: 'add'
            })
          ]
        end

        it 'returns true' do
          expect(issue_resource_object.public_send(method_name)).to be true
        end
      end

      context 'when two in dev label events' do
        let(:label_events) do
          [
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: within_the_range.to_s,
              action: 'add'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: in_dev_label },
              created_at: one_day_ago.to_s,
              action: 'remove'
            }),
            Gitlab::ObjectifiedHash.new({
              label: { name: refinement_label },
              created_at: outside_the_range.to_s,
              action: 'add'
            })
          ]
        end

        it 'returns true' do
          expect(issue_resource_object.public_send(method_name)).to be true
        end
      end

      context 'when time value was not parsed correctly' do
        context 'when nil' do
          let(:label_events) do
            [
              Gitlab::ObjectifiedHash.new({
                label: { name: in_dev_label },
                created_at: nil,
                action: 'add'
              })
            ]
          end

          it 'returns false' do
            expect(issue_resource_object.public_send(method_name)).to be false
          end
        end

        context 'when empty string' do
          let(:label_events) do
            [
              Gitlab::ObjectifiedHash.new({
                label: { name: in_dev_label },
                created_at: '',
                action: 'add'
              })
            ]
          end

          it 'returns false' do
            expect(issue_resource_object.public_send(method_name)).to be false
          end
        end
      end
    end
  end

  describe '#over_one_week_in_dev?' do
    it_behaves_like 'time_in_dev_calculation' do
      let(:method_name) { :over_one_week_in_dev? }
      let(:outside_the_range) { Time.now.utc - (5 * one_day) }
      let(:within_the_range) { Time.now.utc - (10 * one_day) }
    end
  end

  describe '#over_two_weeks_in_dev?' do
    it_behaves_like 'time_in_dev_calculation' do
      let(:method_name) { :over_two_weeks_in_dev? }
      let(:outside_the_range) { Time.now.utc - (10 * one_day) }
      let(:within_the_range) { Time.now.utc - (20 * one_day) }
    end
  end

  describe '#over_three_weeks_in_dev?' do
    it_behaves_like 'time_in_dev_calculation' do
      let(:method_name) { :over_three_weeks_in_dev? }
      let(:outside_the_range) { Time.now.utc - (15 * one_day) }
      let(:within_the_range) { Time.now.utc - (25 * one_day) }
    end
  end

  describe '#over_four_weeks_in_dev?' do
    it_behaves_like 'time_in_dev_calculation' do
      let(:method_name) { :over_four_weeks_in_dev? }
      let(:outside_the_range) { Time.now.utc - (25 * one_day) }
      let(:within_the_range) { Time.now.utc - (30 * one_day) }
    end
  end
end
