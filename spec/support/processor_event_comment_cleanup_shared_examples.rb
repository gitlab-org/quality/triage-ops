# frozen_string_literal: true

RSpec.shared_examples 'event does not require process cleanup' do
  it 'does not require process cleanup' do
    expect(subject.comment_cleanup_applicable?).to be false
  end

  it 'does not call processor_comment_cleanup' do
    allow(subject).to receive(:process)
    expect(subject).not_to receive(:processor_comment_cleanup)

    subject.triage
  end
end

RSpec.shared_examples 'event requires process cleanup' do
  it 'requires process cleanup' do
    expect(subject.comment_cleanup_applicable?).to be true
  end

  it 'calls processor_comment_cleanup' do
    expect(subject).to receive(:processor_comment_cleanup)

    subject.triage
  end
end
