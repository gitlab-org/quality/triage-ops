# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/gdk_issuable_label'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::GdkIssuableLabel do
  [Triage::IssueEvent, Triage::MergeRequestEvent].each do |event_type|
    describe "with #{event_type.name}" do
      include_context 'with event', event_type do
        let(:event_attrs) do
          {
            project_id: Triage::Event::GDK_PROJECT_ID,
            description: description
          }
        end

        let(:description) do
          <<~TEXT
            <!-- Details of the issue. Include any console output or screenshots. -->

            The following categories relate to this issue:

            - [x] ~"gdk-reliability" - e.g. When a GDK action fails to complete.
            - [x] ~"gdk-usability" - e.g. Improvements or suggestions around how the GDK functions.
            - [ ] ~"gdk-performance" - e.g. When a GDK action is slow or times out.
          TEXT
        end
      end

      subject { described_class.new(event) }

      before do
        allow(event).to receive(:with_project_id?).with(Triage::Event::GDK_PROJECT_ID).and_return(true)
      end

      include_examples 'registers listeners', ['issue.open', 'issue.update', 'merge_request.open', 'merge_request.update']

      describe '#documentation' do
        it_behaves_like 'processor documentation is present'
      end

      describe '#applicable?' do
        include_examples 'applicable on contextual event'

        context 'when checkbox selection and labels are matching' do
          let(:label_names) { %w[gdk-reliability gdk-usability] }

          include_examples 'event is not applicable'
        end

        context 'when issuable has a gdk label not selected in the checkbxes' do
          let(:label_names) { ['gdk-performance'] }

          include_examples 'event is applicable'
        end

        context 'when issuable label is partially matching the checkbox selection' do
          let(:label_names) { ['gdk-reliability'] }

          include_examples 'event is applicable'
        end

        context 'when no checkbox is checked, and no gdk labels are applied' do
          let(:description) do
            <<~TEXT
              <!-- Details of the issue. Include any console output or screenshots. -->

              The following categories relate to this issue:

              - [ ] ~"gdk-reliability" - e.g. When a GDK action fails to complete.
              - [ ] ~"gdk-usability" - e.g. Improvements or suggestions around how the GDK functions.
              - [ ] ~"gdk-performance" - e.g. When a GDK action is slow or times out.
            TEXT
          end

          include_examples 'event is not applicable'
        end
      end

      describe '#process' do
        shared_examples 'updating issuable labels' do
          it 'posts comment to update labels' do
            expect_comment_request(event: event, body: expected_comment_body) do
              subject.process
            end
          end
        end

        shared_examples 'not updating issuable labels' do
          it 'does not post comment to update labels' do
            expect_no_request do
              subject.process
            end
          end
        end

        context 'when issuable is missing gdk labels' do
          let(:expected_comment_body) { '/label ~"gdk-reliability" ~"gdk-usability"' }

          it_behaves_like 'updating issuable labels'
        end

        context 'when issuable has extra gdk labels' do
          let(:label_names)           { %w[gdk-reliability gdk-usability gdk-performance] }
          let(:expected_comment_body) { '/unlabel ~"gdk-performance"' }

          it_behaves_like 'updating issuable labels'
        end

        context 'when issuable has an extra gdk label and is missing a gdk label' do
          let(:label_names) { %w[gdk-usability gdk-performance] }
          let(:expected_comment_body) do
            <<~MARKDOWN.chomp
            /label ~"gdk-reliability"
            /unlabel ~"gdk-performance"
            MARKDOWN
          end

          it_behaves_like 'updating issuable labels'
        end

        context 'when issuable has a non gdk category label, it does not get removed' do
          let(:label_names) { %w[gdk-broken gdk-reliability gdk-usability] }

          it_behaves_like 'not updating issuable labels'
        end
      end
    end
  end
end
