# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../../triage/processor/engineering_productivity/pipeline_health/pipeline_incident_sla_enforcer'
require_relative '../../../../triage/triage/event'

RSpec.describe Triage::PipelineHealth::PipelineIncidentSlaEnforcer do
  include_context 'with event', Triage::IncidentEvent do
    let(:is_master_broken_incident_project) { true }
    let(:gitlab_bot_event_actor) { true }
    let(:event_attrs) do
      { from_master_broken_incidents_project?: is_master_broken_incident_project,
        gitlab_bot_event_actor?: gitlab_bot_event_actor,
        added_label_names: label_names }
    end

    let(:label_names) { ['escalation::needed'] }
  end

  subject(:pipeline_incident_sla_enforcer) { described_class.new(event) }

  include_examples 'registers listeners', ['incident.update']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when event is not from master-broken-incident project' do
      let(:is_master_broken_incident_project) { false }

      include_examples 'event is not applicable'
    end

    context 'when event actor is not gitlab-bot' do
      let(:gitlab_bot_event_actor) { false }

      include_examples 'event is not applicable'
    end

    context 'when event does not have master:broken label' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'schedules GroupReminderJob, GroupWarningJob, StageWarningJob and a DevEscalationJob' do
      expect(Triage::PipelineIncidentSla::GroupReminderJob).to receive(:perform_in).with(600, event) # 10 min
      expect(Triage::PipelineIncidentSla::GroupWarningJob).to receive(:perform_in).with(1800, event) # 30 min
      expect(Triage::PipelineIncidentSla::StageWarningJob).to receive(:perform_in).with(13200, event) # 3h 40 min
      expect(Triage::PipelineIncidentSla::DevEscalationJob).to receive(:perform_in).with(14400, event) # 4 hours

      pipeline_incident_sla_enforcer.process
    end
  end
end
