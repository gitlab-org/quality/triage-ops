# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/engineering_productivity/add_quarantine_labels_to_related_issue'
require_relative '../../../triage/triage/event'

RSpec.describe Triage::AddQuarantineLabelsToRelatedIssue do
  let(:project_id) { 12 }
  let(:related_issue_iid) { 42 }

  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: true,
        description: description,
        project_id: project_id,
        label_names: labels
      }
    end

    let(:description) do
      <<~TEXT
      Lorem ipsum

      Related to #42.
      TEXT
    end

    let(:labels) { ['quarantine'] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when MR does not have the ~quarantine label set' do
      let(:labels) { [] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:related_issue_finder) { instance_double(Triage::RelatedIssueFinder) }
    let(:related_issue) { Gitlab::ObjectifiedHash.new(iid: related_issue_iid) }
    let(:issue_edit_api_path) { "/projects/#{project_id}/issues/#{related_issue_iid}" }

    before do
      allow(Triage::RelatedIssueFinder).to receive(:new).and_return(related_issue_finder)
      allow(related_issue_finder).to receive(:find_project_issue_in).and_return(related_issue)
    end

    it 'calls add_quarantine_labels_to_related_issue' do
      expect(subject).to receive(:add_quarantine_labels_to_related_issue)

      subject.process
    end

    it 'adds the labels to the related issue' do
      expect_api_request(
        verb: :put,
        request_body: { add_labels: [Labels::QUARANTINE_LABEL, Labels::QUARANTINE_FLAKY_LABEL] },
        path: issue_edit_api_path,
        response_body: {}
      ) do
        subject.process
      end
    end
  end
end
