# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/code_review_experience_feedback'

RSpec.describe Triage::CodeReviewExperienceFeedback do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        source_path_with_namespace: source_path_with_namespace,
        state: state
      }
    end
  end

  let(:state) { 'merged' }
  let(:source_path_with_namespace) { 'personal/gitlab' }

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.close']

  describe '#applicable?' do
    it_behaves_like 'community contribution processor #applicable?'

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end

    context 'when the merge request is labelled as spam' do
      let(:label_names) { ['Spam'] }

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    context 'when using a community fork and MR merged' do
      let(:state) { 'merged' }
      let(:source_path_with_namespace) { 'gitlab-community/gitlab' }
      let(:body) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            <!-- triage-serverless CodeReviewExperienceFeedback -->
            Hi @joe :wave:

            🎉 See where you rank! Check your spot on the [contributor leaderboard](https://contributors.gitlab.com/leaderboard/me) and unlock rewards.

            ---

            How was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://handbook.gitlab.com/handbook/values/#iteration) and improve:

            1. React with a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
            1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.

            As a benefit of being a GitLab Community Contributor, you have access to GitLab Duo, our AI-powered features.
            With Code Suggestions, Chat, Root Cause Analysis and more AI-powered features, GitLab Duo helps to boost your efficiency and effectiveness
            by reducing the time required to write and understand code and pipelines.
            Visit the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/gitlab_duo/) to learn more about the benefits.

            Thanks for your help! :heart:
          MARKDOWN
        end
      end

      it 'posts code review experience message' do
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when not using a community fork and MR not merged' do
      let(:state) { 'closed' }
      let(:body) do
        add_automation_suffix do
          <<~MARKDOWN.chomp
            <!-- triage-serverless CodeReviewExperienceFeedback -->
            Hi @joe :wave:

            How was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://handbook.gitlab.com/handbook/values/#iteration) and improve:

            1. React with a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
            1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.

            [Request access](https://gitlab.com/groups/gitlab-community/community-members/-/group_members/request_access) to our community forks to receive complimentary access to GitLab Duo, our AI-powered features.
            With Code Suggestions, Chat, Root Cause Analysis and more AI-powered features, GitLab Duo helps to boost your efficiency and effectiveness
            by reducing the time required to write and understand code and pipelines.
            Visit the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/gitlab_duo/) to learn more about the benefits.

            🎉 See where you rank! Check your spot on the [contributor leaderboard](https://contributors.gitlab.com/leaderboard/me) and unlock rewards.

            Thanks for your help! :heart:
          MARKDOWN
        end
      end

      it 'posts code review experience message' do
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
