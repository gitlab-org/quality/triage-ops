# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/triage/event'
require_relative '../../../triage/processor/optimize_group/feature_mr_seeding_script_reminder'

RSpec.describe Triage::FeatureMrSeedingScriptReminder do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        from_gitlab_org_gitlab?: from_gitlab_or_gitlab,
        iid: mr_iid
      }
    end

    let(:from_gitlab_or_gitlab) { true }
    let(:label_names) { ['group::optimize', 'feature::addition'] }
    let(:previous_discussion) { false }
    let(:mr_iid) { 1234 }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.open', 'merge_request.update']

  describe '#applicable?' do
    before do
      allow_any_instance_of(Triage::UniqueComment).to receive(:previous_discussion).and_return(previous_discussion)
    end

    include_examples 'applicable on contextual event'

    context 'when not from gitlab-org/gitlab' do
      let(:from_gitlab_or_gitlab) { false }

      it_behaves_like 'event is not applicable'
    end

    context 'when some required labels are missing' do
      let(:label_names) { ['group::optimize', 'bug::performance'] }

      it_behaves_like 'event is not applicable'
    end

    context 'when previous discussion exists' do
      let(:previous_discussion) { true }

      it_behaves_like 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'adds a discussion with the correct body' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
          <!-- triage-serverless FeatureMrSeedingScriptReminder -->
          :wave: Hi @#{event.resource_author.username},

          Please ensure **data seeding scripts** are created and/or updated by following the [optimize group handbook page](https://handbook.gitlab.com/handbook/engineering/development/dev/plan/optimize/#data-seeding-scripts).
        MARKDOWN
      end

      expect_discussion_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
